#!/bin/sh

ENDPOINT=`jq -r .endpoint /data/options.json`
KEY=`jq -r .key /data/options.json`
SECRET=`jq -r .secret /data/options.json`
BUCKET=`jq -r .bucketname /data/options.json`

now="$(date +'%d/%m/%Y - %H:%M:%S')"

echo $now

mc alias set storage ${ENDPOINT} ${KEY} ${SECRET}
mc mirror /backup/ storage/${BUCKET}

echo "Done"
