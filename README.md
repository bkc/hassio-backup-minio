# Hassio Addon for Backing up to Minio Bucket

Add-on for uploading hass.io snapshots to Minio.

## Installation

Under the Add-on Store tab in the Hass.io Supervisor view in HA add this repo as an add-on repository: `https://gitlab.com/bkc/hassio-backup-minio`.

Install, then set the config variables that you obtained from setting up the Minio account, user and bucket (see below):
key: `access key id`
secret: `secret access key`
endpoint: `Minio endpoint`
bucketname: `Minio bucket name`

## Usage

To sync your HASSIO backup folder with Minio just click START in this add-on. It will keep a synced cloud-copy, so any purged backup files will not be kept in your bucket either.

You could automate this using Automation:

```yaml
# backups
- alias: Make snapshot
  trigger:
    platform: time
    at: '3:00:00'
  condition:
    condition: time
    weekday:
      - mon
  action:
    service: hassio.snapshot_full
    data_template:
      name: Automated Backup {{ now().strftime('%Y-%m-%d') }}

- alias: Upload to Minio
  trigger:
    platform: time
    at: '3:30:00'
  condition:
    condition: time
    weekday:
      - mon
  action:
    service: hassio.addon_start
    data:
      addon: local_backup_minio
```

The automation above first makes a snapshot at 3am, and then at 3.30am uploads to Minio.

## Help and Debug

Please post an issue on this repo with your full log.

## Alternative Backup solution

I really like this community integration too:
https://github.com/jcwillox/hass-auto-backup

Once installed, it can be easily adapted to run alongside this addon.

Contact: hello@mikebell.io

Credits: Based on jperquin/hassio-backup-s3 based on rrostt/hassio-backup-s3
